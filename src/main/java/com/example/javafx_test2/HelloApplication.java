package com.example.javafx_test2;

import com.example.javafx_test2.controller.MainController;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class HelloApplication extends Application {

    private static final int DEFAULT_WIDTH_SIZE = 1200;
    private static final int DEFAULT_HEIGHT_SIZE = 800;

    @Override
    public void start(Stage stage) {
        MainController mainController = new MainController();
        Scene scene = new Scene(mainController.getRootScene(), DEFAULT_WIDTH_SIZE, DEFAULT_HEIGHT_SIZE);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}