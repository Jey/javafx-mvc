package com.example.javafx_test2.controller;

import com.example.javafx_test2.model.ExampleModel;
import com.example.javafx_test2.view.MainView;
import com.example.javafx_test2.view.dynamicview.DynamicViewEnum;
import javafx.scene.layout.Region;

public class MainController {

  private final MainView mainView;
  private final ExampleModel exampleModel;

  public MainController() {
    this.mainView = new MainView(this);
    this.exampleModel = new ExampleModel();
  }

  public Region getRootScene() {
    return mainView.build();
  }

  public void onMenu1Click() {
    mainView.updateDynamicView(DynamicViewEnum.MENU_1);
  }

  public void onMenu2Click() {
    mainView.updateDynamicView(DynamicViewEnum.MENU_2);
  }

  public void onMenu1OkButtonClick() {
    exampleModel.compute(mainView.getMenu1().getRealTextFieldContent());
  }

  public void toggleMenu() {
    mainView.toggleMenu();
  }
}
