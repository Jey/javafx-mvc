package com.example.javafx_test2.view.navigation;

import com.example.javafx_test2.controller.MainController;
import com.example.javafx_test2.view.AbstractView;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

public class MenuView extends AbstractView {

  private static final int MENU_SPACING = 10;
  private static final Insets MENU_PADDING = new Insets(8);

  private final MainController controller;

  public MenuView(MainController mainController) {
    this.controller = mainController;
  }

  @Override
  protected Node generateView() {
    VBox navbar = new VBox(MENU_SPACING);
    navbar.setPadding(MENU_PADDING);

    Button menu1 = createButton("menu1");
    menu1.setOnMouseClicked((event) -> controller.onMenu1Click());
    Button menu2 = createButton("menu2");
    menu2.setOnMouseClicked((event) -> controller.onMenu2Click());

    navbar.getChildren().addAll(menu1, menu2);

    return navbar;
  }

  private Button createButton(String title) {
    Button button = new Button(title);
    // button.setBackground(Background.EMPTY); --> rend les boutons transparents
    return button;
  }
}
