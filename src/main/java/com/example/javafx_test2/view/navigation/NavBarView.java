package com.example.javafx_test2.view.navigation;

import com.example.javafx_test2.controller.MainController;
import com.example.javafx_test2.view.AbstractView;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

public class NavBarView extends AbstractView {

  private static final int NAVBAR_SPACING = 10;
  private static final Insets NAVBAR_PADDING = new Insets(8);

  private final MainController controller;

  public NavBarView(MainController mainController) {
    this.controller = mainController;
  }

  @Override
  protected Node generateView() {
    HBox navbar = new HBox(NAVBAR_SPACING);
    navbar.setPadding(NAVBAR_PADDING);

    Button menuEnabler = createButton("Menu");
    menuEnabler.setOnMouseClicked(mouseEvent -> controller.toggleMenu());
    Button settings = createButton("Paramètres");
    Button about = createButton("À propos");

    navbar.getChildren().addAll(menuEnabler, settings, about);

    return navbar;
  }

  private Button createButton(String title) {
    Button button = new Button(title);
    // button.setBackground(Background.EMPTY); --> rend les boutons transparents
    return button;
  }
}
