package com.example.javafx_test2.view;

import com.example.javafx_test2.controller.MainController;
import com.example.javafx_test2.view.navigation.MenuView;
import com.example.javafx_test2.view.navigation.NavBarView;
import com.example.javafx_test2.view.dynamicview.DynamicViewEnum;
import com.example.javafx_test2.view.dynamicview.Menu1View;
import com.example.javafx_test2.view.dynamicview.Menu2View;
import javafx.animation.TranslateTransition;
import javafx.scene.Parent;
import javafx.scene.layout.*;
import javafx.util.Builder;
import javafx.util.Duration;

public class MainView extends Parent implements Builder<Region> {

  // statics views
  private final BorderPane rootLayout;
  private final NavBarView navBarView;
  private final MenuView menuView;

  // dynamics views
  private final Menu1View menu1;
  private final Menu2View menu2;

  // config
  private TranslateTransition menuAnimation;

  // states
  private boolean isMenuEnable = true;

  public MainView(MainController mainController) {
    // statics views
    this.rootLayout = new BorderPane();
    this.navBarView = new NavBarView(mainController);
    this.menuView = new MenuView(mainController);

    // dynamics views
    this.menu1 = new Menu1View(mainController);
    this.menu2 = new Menu2View(mainController);
  }

  @Override
  public Region build() {
    rootLayout.setTop(navBarView.getView());
    rootLayout.setLeft(menuView.getView());

    this.menuAnimation = new TranslateTransition(Duration.millis(500), menuView.getView());
    menuAnimation.setFromX(0);
    menuAnimation.setToX(-190);
    menuAnimation.setOnFinished(actionEvent -> {
      if (isMenuEnable) {
        isMenuEnable = false;
        menuView.getView().setVisible(isMenuEnable);
        menuView.getView().managedProperty().bind(menuView.getView().visibleProperty());
      } else {
        isMenuEnable = true;
      }
    });

    return rootLayout;
  }

  public void updateDynamicView(DynamicViewEnum dynamicViewEnum) {
    switch(dynamicViewEnum) {
      case MENU_1:
        rootLayout.setCenter(menu1.getView());
        break;
      case MENU_2:
        rootLayout.setCenter(menu2.getView());
        break;
      default:
        throw new RuntimeException("Menu option not implemented yet");
    }
  }

  public Menu1View getMenu1() {
    return menu1;
  }

  public Menu2View getMenu2() {
    return menu2;
  }

  public void toggleMenu() {
    if (isMenuEnable) {
      menuAnimation.setRate(1);
      menuAnimation.play();
    } else {
      menuView.getView().setVisible(!isMenuEnable);
      menuView.getView().managedProperty().bind(menuView.getView().visibleProperty());
      menuAnimation.setRate(-1);
      menuAnimation.play();
    }
  }

  /*private Node createHeadingBox() {
    HBox results = new HBox();
    Text headingText = new Text("This is the Screen Title");
    headingText.getStyleClass().add("heading-text");
    results.getStyleClass().add("standard-border");
    results.getChildren().add(headingText);
    results.setPadding(new Insets(6));
    return results;
  }

  private Node createDataEntrySection() {
    HBox results = new HBox(10);
    results.setPadding(new Insets(2, 10, 4, 2));
    results.getChildren().addAll(createGridPane(), createBioBox());
    return results;
  }

  private Node createBioBox() {
    TextArea bioTextArea = new TextArea();
    bioTextArea.setWrapText(true);
    bioTextArea.setMaxWidth(200);
    bioTextArea.setMaxHeight(200);
    bioTextArea.textProperty().bindBidirectional(model.bioProperty());
    return new VBox(4, createPromptText("Short Bio:"), bioTextArea);
  }

  private Node createGridPane() {
    GridPane results = createTwoColumnGridPane();
    results.add(createPromptText("First Name:"), 0, 0);
    results.add(createInputField(model.firstNameProperty()), 1, 0);
    results.add(createPromptText("Last Name:"), 0, 1);
    results.add(createInputField(model.lastNameProperty()), 1, 1);
    results.add(createPromptText("Email:"), 0, 2);
    results.add(createInputField(model.emailProperty()), 1, 2);
    results.add(createPromptText("Phone Number:"), 0, 3);
    results.add(createInputField(model.phoneProperty()), 1, 3);
    return results;
  }

  private Node createButtonBox() {
    HBox results = new HBox();
    results.setAlignment(Pos.CENTER_RIGHT);
    results.setPadding(new Insets(8));
    Button button = new Button("Save");
    results.getChildren().add(button);
    return results;
  }

  private GridPane createTwoColumnGridPane() {
    GridPane results = new GridPane();
    results.getColumnConstraints().addAll(createJustifiedConstraint(HPos.RIGHT), createJustifiedConstraint(HPos.LEFT));
    results.setHgap(6);
    results.setVgap(4);
    results.setPadding(new Insets(4));
    results.getStyleClass().add("test-border");
    return results;
  }

  private ColumnConstraints createJustifiedConstraint(HPos alignment) {
    ColumnConstraints results = new ColumnConstraints();
    results.setHalignment(alignment);
    return results;
  }

  private Node createInputField(StringProperty boundProperty) {
    TextField results = new TextField();
    results.setMinWidth(100);
    results.textProperty().bindBidirectional(boundProperty);
    return results;
  }

  private Node createPromptText(String prompt) {
    Text results = new Text(prompt);
    results.getStyleClass().add("label-text");
    return results;
  }*/
}