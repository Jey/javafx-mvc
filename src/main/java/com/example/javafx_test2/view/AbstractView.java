package com.example.javafx_test2.view;

import javafx.scene.Node;

public abstract class AbstractView {

  protected Node view;

  public Node getView() {
    if (view == null) {
      this.view = generateView();
    }
    return view;
  }

  protected abstract Node generateView();
}
