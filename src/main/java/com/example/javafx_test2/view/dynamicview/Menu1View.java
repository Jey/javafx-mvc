package com.example.javafx_test2.view.dynamicview;

import com.example.javafx_test2.controller.MainController;
import com.example.javafx_test2.view.AbstractView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.Text;

import java.util.List;

public class Menu1View extends AbstractView {

  private final MainController controller;

  private TextField realTextField;

  public Menu1View(MainController mainController) {
    this.controller = mainController;
  }

  @Override
  protected Node generateView() {
    BorderPane mainLayout = new BorderPane();

    HBox titleLayout = new HBox();
    Text title = new Text("Menu 1");
    titleLayout.getChildren().add(title);
    titleLayout.setAlignment(Pos.CENTER);
    titleLayout.setPadding(new Insets(20));
    mainLayout.setTop(titleLayout);

    Text realText = new Text("Vrai text field : ");
    realTextField = new TextField("test");
    HBox okButtonLayout = new HBox();
    Button okButton = new Button("On envoie les données au modèle (en passant par le controller)");
    okButton.setOnMouseClicked((mouseEvent -> controller.onMenu1OkButtonClick()));
    okButtonLayout.setAlignment(Pos.CENTER_RIGHT);
    okButtonLayout.getChildren().add(okButton);
    VBox contentLayout = new VBox(3);
    contentLayout.setPadding(new Insets(20));
    contentLayout.getChildren().addAll(addTooMuchComponents());
    contentLayout.getChildren().addAll(
        realText,
        realTextField,
        okButtonLayout
    );
    mainLayout.setCenter(contentLayout);

    ScrollPane sp = new ScrollPane(mainLayout);
    sp.setStyle("-fx-background-color:transparent;");
    sp.setFitToWidth(true);
    sp.setFitToHeight(true);

    return sp;
  }

  public String getRealTextFieldContent() {
    return realTextField.getText();
  }

  private List<Node> addTooMuchComponents() {
    return List.of(
        new Text("Input :"),
        new TextField(),
        new Text("Input :"),
        new TextField(),
        new Text("Input :"),
        new TextField(),
        new Text("Input :"),
        new TextField(),
        new Text("Input :"),
        new TextField(),
        new Text("Input :"),
        new TextField(),
        new Text("Input :"),
        new TextField(),
        new Text("Input :"),
        new TextField(),
        new Text("Input :"),
        new TextField(),
        new Text("Input :"),
        new TextField(),
        new Text("Input :"),
        new TextField(),
        new Text("Input :"),
        new TextField(),
        new Text("Input :"),
        new TextField(),
        new Text("Input :"),
        new TextField(),
        new Text("Input :"),
        new TextField()
    );
  }
}
