package com.example.javafx_test2.view.dynamicview;

import com.example.javafx_test2.controller.MainController;
import com.example.javafx_test2.view.AbstractView;
import javafx.scene.Node;
import javafx.scene.text.Text;

public class Menu2View extends AbstractView {
  public Menu2View(MainController mainController) {
  }

  @Override
  protected Node generateView() {
    return new Text("Menu2View");
  }
}
